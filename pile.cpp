#include "pile.h"

void add(Card &c)
{
    war_pile.push_back(c);
}

void award_spoils(Player &p)
{
    for(int i = 0; i < war_pile.size(); i++)
    {
        p.add(war_pile[i]);
    }
    war_pile.clear();
}
