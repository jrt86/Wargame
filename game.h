#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#include "deck.h"
#include "player.h"
#include "pile.h"

struct Game
{
    Game(){deck = make_standard_deck();}

    void Run();
    Deck deck;
    Player p1;
    Player p2;
    Pile pile;
    int turn;

};

#endif // GAME_H_INCLUDED
