#include "Deck.h"
#include <algorithm>
#include <random>
#include <ctime>


void Deck::createDeck()
{
    //Add first 52 cards
    for (int i = Hearts; i <= Spades; i++)
    {
        for (int j = Ace; j <= King; j++)
        {
//            Suited card{static_cast<Rank>(j), static_cast<Suit>(i)};
//            deck.push_back(&card);
            deck.push_back(new Suited {static_cast<Rank>(j), static_cast<Suit>(i)});
        }
    }
    //Add two joker cards
    for(int k = Red; k <= Black; k++)
    {
        deck.push_back(new Joker {static_cast<Color>(k)});
    }
}

void Deck::printType()
{
    for(auto d : deck)
    {
        d->printType();
    }

}

void Deck::printRank()
{
    for(auto d : deck)
    {
        d->printRank();
    }

}

void Deck::shuffle()
{
    std::random_shuffle(deck.begin(), deck.end());
}

