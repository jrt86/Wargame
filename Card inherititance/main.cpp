#include <random>
#include "Card.h"
#include "Deck.h"


int main()
{
    Deck deck;
    deck.createDeck();
    deck.shuffle();
    deck.printRank();
    //deck.printType();
    return 0;
}
