#ifndef DECK_H_INCLUDED
#define DECK_H_INCLUDED
#include "Card.cpp"
#include <vector>

class Deck
{
public:
    Deck(){deck.reserve(54);};
    std::vector<Card*> deck;
    void createDeck();
    void printType();
    void printRank();
    void shuffle();
};

#endif // DECK_H_INCLUDED
