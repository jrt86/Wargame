#ifndef CARD_H_INCLUDED
#define CARD_H_INCLUDED

#include <iostream>
#include <stdexcept>
enum Rank
{
  Ace,
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Ten,
  Jack,
  Queen,
  King,
};

enum Suit
{
  Hearts,
  Diamonds,
  Clubs,
  Spades,
};

enum Color
{
    Red,
    Black,
};

class Card
{
    //virtual ~Card(){};
public:
    virtual void printType()
    {std::cout << "Card\n";}
    virtual void printRank()
    {throw std::logic_error("This shouldn't happen.\n");}
};

class Suited : public Card
{
public:
    void printType()
    {std::cout << "Suited\n";};
    void printRank()
    {std::cout << rank << ' ' <<  suit << '\n';}
    Suited() : rank(Ace), suit(Spades)
    {};
    Suited(Rank r, Suit s) : rank(r), suit(s)
    {};
    Rank rank;
    Suit suit;
};

class Joker : public Card
{
public:
    void printType()
    {std::cout << "Joker\n";}
    void printRank()
    {std::cout << color << '\n';}
    Joker() : color(Red)
    {};
    Joker(Color c) : color(c)
    {};
    Color color;
};

#endif // CARD_H_INCLUDED
