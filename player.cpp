#include "player.h"


//Default constructor
Player()
{
    player_pile.reserve(52);
}

//Add cards to the player
void add(Card &c)
{
    player_pile.push_back(c);
}

//return false if one player has the entire deck
bool check_if_won()
{
    if(player_pile.size() == 52)
    {
        return false;
    }
    return true;
}

Card play_card()
{
    Card c = player_pile.front();
    player_pile.erase(0);
    return c;
}
