#include "Player.hpp"
#include "Card.hpp"
#include "Deck.hpp"

#include <iostream>

void Player::displayHand(bool hidden) {
    if (hidden)
        std::cout << "X "; // Upside down card will be represented by an 'X'

    int startIndex;
    if (hidden) startIndex = 1; // Skip the first card if it's unknown
    else startIndex = 0;

    for (int i = startIndex; i < hand.size(); i++)
        std::cout << hand[i] << " ";
    std::cout << std::endl;
}

void Player::takeCards(int num, Deck &deck) {
    for (int i = 0; i < num; i++) {
        if (deck.back().getInfo().sc.getRank() == Ace) numHighAces++;

        hand.push_back(deck.back());
        deck.pop_back();
    }
}

int Player::countPoints() {
    int points = 0;
    for (Card c : hand)
        points += c.points;

    int highAces = numHighAces;
    while (points > 21 && highAces > 0) { // Repeat the action in the comment below until the player is <=21 or no more 11pt Aces
        points -= 10; // Essentially take a single ace card and change its point value from 11 to 1
        highAces--; // Indicate that there is now one less 11pt Ace
    }

    return points;
}
