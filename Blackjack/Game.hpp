#ifndef GAME_HPP
#define GAME_HPP

#include <vector>
#include "Player.hpp"
#include "Deck.hpp"

struct Options {
    int numOfPlayers;
    int numOfDecks;

    const int startingMoney = 100;
};

class Game
{
    public:
        Game();
        //void Step(Deck&);
        void Run();

        Options options;

    private:
        Deck deck;
        std::vector<Player> players;
        Player dealer;

        void takeTurn(Player &player);
        void Step();
        void dealerTurn();
        bool cleanUp();
};


#endif // GAME_HPP
