#include "card.hpp"
#include "deck.hpp"
#include "Game.hpp"

#include <iostream>
#include <iomanip>

int main()
{
    //srand(time(0)); // Need to see for the shuffle method

    Game game;
    game.Run();

    return 0;
}
