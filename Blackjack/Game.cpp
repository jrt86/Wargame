#include "Game.hpp"
#include "Deck.hpp"
#include "Player.hpp"
#include <iostream>
#include <locale>

Game::Game()
{
    //ctor
}

void Game::takeTurn(Player &player) {
    bool hit = true;
    char choice;

    std::cout << "Dealer's hand: ";
    dealer.displayHand(true);
    std::cout << std::endl;

    while (hit) {
        std::cout << "Hand: ";
        player.displayHand(false);
        std::cout << "Points : " << player.countPoints() << "\n";

        do {
            std::cout << "\n[H]it or [S]tand: ";
            std::cin >> choice;
            choice = toupper(choice); // Going to ignore case
        } while (choice != 'H' && choice != 'S');

        if (choice == 'H') {
            player.takeCards(1, deck);
            if (player.countPoints() > 21) {
                std::cout << "Hand: ";
                player.displayHand(false);
                std::cout << "Points : " << player.countPoints() << "\n";

                std::cout << "Player " << player.getId() << " busted!\n";
                hit = false;
            }
        } else {
            std::cout << "Player " << player.getId() << " stands at " << player.countPoints() << ".\n";
            hit = false;
        }
    }
}

/*
- Dealer must hit until reaching 16+ points. After reaching at least 16, he will automatically stand
- Add messages for dealer to match takeTurn function
- Go through player array and give each winner double what is in their 'bet' property, then set it back to 0
- Add appropriate messages. ex "Dealer stopped at 18. Player 1 wins 100 tokens with 19 points"
*/

void Game::dealerTurn() {

    if(dealer.countPoints() >= 16)
    {
        dealer.takeCards(1, deck);
    }
    else
    {
        if(dealer.countPoints() > 21)
        {
            std::cout << "Dealer has busted\n";
        }
        else
        {
            std::cout << "Dealer stands at " << dealer.countPoints() << "\n";
        }
    }
}

void Game::Run()
{
    //Deck deck = Deck();
    deck = Deck();
    int roundCount = 1;

    //Changed wording to keep it consistent
    std::cout << "Number of decks: ";
    std::cin >> options.numOfDecks;
    while(!std::cin)
    {
        std::cout << "Please enter a valid integer: ";
        std::cin.clear();
        std::cin.ignore();
        std::cin >> options.numOfDecks;
    }

    //Create desired amount of decks
    for(int i = 0; i < options.numOfDecks; i++) {
        deck = makeCombinedDeck(deck, makeStandardDeck());
    }

    std::cout << "Number of players: ";
    std::cin >> options.numOfPlayers;
    while(!std::cin)
    {
        std::cout << "Please enter a valid integer: ";
        std::cin.clear();
        std::cin.ignore();
        std::cin >> options.numOfPlayers;
    }

    // Create each player and push them into the array
    players.reserve(options.numOfPlayers);                        // Id is i + 1 so it starts with player 1
    for(int i = 0; i < options.numOfPlayers; i++) {
        players.push_back(Player(i + 1, options.startingMoney));
    }

    bool flag;

    do
    {
        std::cout << "\nBegin round " << roundCount << "\n";
        roundCount += 1;

        Step();
        flag = cleanUp();
    }while(flag);
}

void Game::Step()
{
    bool gameWon = false;
    bool valid = false;
    char anotherCard;

    shuffle(deck);

    //takeTurn(dealer);
    dealer.takeCards(2, deck);

    //for(int i = 0; i < numOfPlayers; i++)
    for (Player &player : players)
    {
        //players[i].id = i;
        //std::cout << "Place your bet:  \n";

        std::cout << "\nPlayer " << player.getId() << " money: " << player.money << std::endl;
        std::cout << "Place your bet:  ";
        std::cin >> player.bet;
        //players[i].money = bet;
        player.money -= player.bet;
        std::cout << "Money left: " << player.money << "\n\n";

        player.takeCards(2, deck);

        takeTurn(player);
        /*
        std::cout << "Would you like another card? (Y)/(N) \n";
        std::cin >> anotherCard;
        while(!valid)
        if(anotherCard == 'Y' || 'y')
        {
            players[i].takeCard(deckOfCards);
            takeTurn(players[i]);
            valid = true;
        }
        else if(anotherCard == 'N' || 'n')
        {
            takeTurn(players[i]);
            valid = true;
        }
        else
        {
            std::cout << "Please enter a valid character (Y) || (y)";
            std::cin >> anotherCard;
        }
        */

        /*
        while(!gameWon)
        {
            if (dealer.countPoints() < player.countPoints())
            {
                std::cout << "You have won! \n";
                gameWon = true;
            }
            else if (player.countPoints() < dealer.countPoints())
            {
                std::cout << "You have lost to the dealer! \n";
            }

            else if (player.countPoints() == dealer.countPoints())
            {
                std::cout << "Tie! \n";
            }
        }
        */
    }

    dealerTurn();

}

/*
- Remove any player with a negative balance from the game
- Remove the cards from everyone's hands(including dealer) and put them back in the deck
*/
bool Game::cleanUp() {
    char choice;

    /*
    do {
        std::cout << "Start another round? (y/n)\n";
        std::cin >> choice;
        choice = tolower(choice);

        if(choice == 'n')
            Flag = false;
        else if(choice == 'y')
            Flag = true;
        else
            std::cout << "Invalid choice.\n";

    }while(choice != 'y' && choice != 'n');
    */

    std::cout << "Start another round? (y/n)\n";
    std::cin >> choice;
    choice = tolower(choice);

    while(!std::cin || !(choice == 'n' ||  choice == 'y'))
    {
        std::cout << "Please enter a valid option: ";
        std::cin.clear();
        std::cin.ignore();
        std::cin >> choice;
    }

    if(choice == 'y') {
        //- Remove any player with a negative balance from the game
        return true;
    }

    return false;
}
