#include "card.hpp"

#include <vector>

using Deck = std::vector<Card>; // The top of the deck, when upside down, will be the last element

Deck makeStandardDeck();
Deck makeCombinedDeck(const Deck &d1, const Deck &d2);
void shuffle(Deck &deck);
void print(const Deck &deck);

