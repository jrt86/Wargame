#include "deck.hpp"

#include <iostream>
#include <random>
#include <algorithm>

Deck makeStandardDeck() {
    Deck d;
    d.reserve(52);

    // Much easier to just loop through the enums to create the deck instead of doing it manually
    for (int suitInt = Hearts; suitInt <= Spades; suitInt++) {
        for (int rankInt = Ace; rankInt <= King; rankInt++) {
            Card c = {Suited, .info={.sc={static_cast<Rank>(rankInt), static_cast<Suit>(suitInt)}}};
            switch (static_cast<Rank>(rankInt)) {
                case Ace:
                    c.points = 11;
                    break;
                case Two:
                case Three:
                case Four:
                case Five:
                case Six:
                case Seven:
                case Eight:
                case Nine:
                    c.points = rankInt + 1;
                    break;
                case Ten:
                case Jack:
                case Queen:
                case King:
                    c.points = 10;
                    break;
            }
            d.push_back(c);
        }
    }

    return d;
}

Deck
makeCombinedDeck(const Deck& d1, const Deck& d2)
{
    Deck d;
    d.insert(d.end(), d1.begin(), d1.end());
    d.insert(d.end(), d2.begin(), d2.end());
    return d;
}

void
shuffle(Deck& d)
{
    // Make sure to seed somewhere in the program first or else it'll shuffle in the same way every time
    std::random_shuffle(d.begin(), d.end());
}

void
print(const Deck& deck)
{
    int i = 1;
    for (Card c : deck) {
        std::cout << c << ' ';
        if (i % 13 == 0) {
            std::cout << '\n';
            i = 0;
        }
        ++i;
    }

    std::cout << '\n';
}

