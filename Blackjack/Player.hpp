#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "Card.hpp"
#include "Deck.hpp"

#include <vector>

class Player
{
    public:
        Player() = default;
        Player(int i, int m) : id(i), money(m), bet(0) {}

        void displayHand(bool hidden); // hidden - is the first card unknown or not?
        void takeCards(int num, Deck &deck); // Give player the top card from the deck
        int countPoints(); // Returns total value of player's cards

        int getId() {return id;}

        int id; // Track the player's number. Will start at 0
        int money; // Will keep track of amount of money a player has
        int bet; // Amount wagered

        std::vector<Card> hand; // The player's current cards   MAKE PRIVATE

    private:
        int numHighAces = 0; //Bit of a hack. Used so program can make an Ace worth 1 instead of 11 in the case of a bust

};

#endif // PLAYER_HPP
