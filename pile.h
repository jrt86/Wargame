#ifndef PILE_H_INCLUDED
#define PILE_H_INCLUDED

#include "deck.h"

class Pile
{
public:
    Pile(){war_pile.reserve(52);}
    void add(Card &c);
    void award_spoils(Player &p);
private:
    Deck war_pile;
};

#endif // PILE_H_INCLUDED
