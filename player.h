#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED

#include <vector>
#include "deck.h"

using Deck = std::vector<Card>;
class Player
{
public:
    Player();
    bool check_if_won();
    void add(Card &c);
    Card play_card();

private:
    Deck player_pile;
};

#endif // PLAYER_H_INCLUDED
