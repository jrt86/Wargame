#include "json.hpp"

#include <iostream>
#include <string>
#include <fstream>

int
main() {

std::ofstream outFile;
outFile.open("JSON.txt");

Value* v = new Array {
    new Null{},
    new Bool{true},
    new Bool{false},
    new Array{
        new Null{},
        new Bool{true},
        new Bool{true},
        new Bool{true},
        new Bool{false},
        new Bool{true},
        new Bool{true},
        new Number{52.1234},
        new Array{
            new Number{3.141592653589793},
            new String{"..."},
            new Array{},
        }
    },
    new String{"Hello"}
};
v->print(outFile);
delete v;
outFile.close();
std::cout << "Finished!";
//Value* b = new Bool(true);
//b->print();
}
