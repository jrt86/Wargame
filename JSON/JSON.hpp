#ifndef JSON_HPP_INCLUDED
#define JSON_HPP_INCLUDED

#include <iostream>
#include <vector>
#include <map>
#include <fstream>

// Represents the base class for all JSON values
struct Value
{
    virtual ~Value() {}
    virtual void print(std::ofstream&) = 0;
};

//Represents either a true or false value
struct Bool : Value
{
    bool value;
    Bool(bool val) : value(val) {}
    virtual void print(std::ofstream&);
};

//Represents a null value
struct Null : Value
{
    Null() {}
    virtual void print(std::ofstream& output)
    {output << "Null";}
};

//Represents a string
struct String : Value
{
    std::string strng;
    String(std::string s) : strng(s) {}
    virtual void print(std::ofstream&);
};

//Represents an array
struct Array : Value
{
    std::vector<Value*> arr;
    Array() {}
    Array(std::initializer_list<Value*> init) : arr(init) {}
    virtual ~Array();
    virtual void print(std::ofstream&);
};

//Represents an object
struct Object : Value
{
  Object() {};
  ~Object();
  virtual void print(std::ofstream&);
  std::map<String*, Value*> fields;
};

//Represents a floating point value
struct Number : Value
{
  double value;
  Number(double num) : value(num) {}
  virtual void print(std::ofstream& output)
  {output << value;}
};

#endif // JSON_HPP_INCLUDED
