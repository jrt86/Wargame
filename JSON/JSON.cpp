#include "JSON.hpp"

#include <iostream>
#include <fstream>

void Bool::print(std::ofstream& output)
{
    if(value)
        output << "True";
    else
        output << "False";
}

void String::print(std::ofstream& output)
{
    output << strng ;
}

void Array::print(std::ofstream& output)
{
    output << "[";
    for(size_t i = 0; i < arr.size(); ++i)
    {
       arr[i]->print(output);
       if(i + 1 != arr.size())
       {
           output << ", ";
       }
    }
    output << "]";
}

void Object::print(std::ofstream& output)
{
    output << '{';
    for (auto iter = fields.begin(); iter != fields.end(); ++iter) {
      iter->first->print(output);
      output << " : ";
      iter->second->print(output);
      if (std::next(iter) != fields.end())
      {
        output << ", ";
      }
    }
}
//    for(auto const &map1 : fields)
//    {
//        map1.first->print(output);
//        output << " : ";
//        map1.second->print(output);
//        //Can't verify the end of the loop
//`       output << ", ";
//    }
//    output << "}";
//}

Array::~Array()
{
    for(Value* v : arr)
    {
        delete v;
    }
}

Object::~Object()
{
    for (auto p : fields)
    {
      delete p.first;
      delete p.second;
    }
}
