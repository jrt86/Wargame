#include <iostream>

#include "VisitCalc.h"

int main()
{

  //Both examples use the visitor pattern
  Expr* test1 = new Sub(new Int(5), new Add(new Int(5), new Int(6)));

  EvalVisitor j;
  test1->accept(j);

  print(test1);
  std:: cout << " = " << eval(test1) << '\n';

  Expr* test2 = new Mul(new Int(5), new Div(new Int(120), new Int(6)));

  EvalVisitor k;
  test2->accept(k);

  print(test2);
  std:: cout << " = " << eval(test2);

  delete test1;
  delete test2;
  return 0;
}
