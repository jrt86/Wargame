#ifndef VISITCALC_H_INCLUDED
#define VISITCALC_H_INCLUDED

#include <stdexcept>
#include <iostream>

struct Expr;
struct Int;
struct Add;
struct Sub;
struct Mul;
struct Div;

struct Visitor
{
  virtual void visit(Int* e) = 0;
  virtual void visit(Add* e) = 0;
  virtual void visit(Sub* e) = 0;
  virtual void visit(Mul* e) = 0;
  virtual void visit(Div* e) = 0;
};

struct EvalVisitor : Visitor
{
  int ret;

  virtual void visit(Int* e);
  virtual void visit(Add* e);
  virtual void visit(Sub* e);
  virtual void visit(Mul* e);
  virtual void visit(Div* e);

};

int eval(Expr* e);
void print(Expr* e);

struct Expr
{
  virtual ~Expr() = default;

  virtual void accept(Visitor& v) = 0;
};

struct Int : Expr
{
  Int(int n) : val(n) { }

  virtual void accept(Visitor& v) { v.visit(this); }

  int val;
};

struct Binary : Expr
{
  Binary(Expr *e1, Expr* e2) : e1(e1), e2(e2) { }

  virtual ~Binary()
  {
    delete e1;
    delete e2;
  }

  Expr* e1;
  Expr* e2;
};

struct Add : Binary
{
  using Binary::Binary;

  virtual void accept(Visitor& v) { v.visit(this); }
};

struct Sub : Binary
{
  using Binary::Binary;

  virtual void accept(Visitor& v) { v.visit(this); }
};

struct Mul : Binary
{
  using Binary::Binary;

  virtual void accept(Visitor& v) { v.visit(this); }
};

struct Div : Binary
{
  using Binary::Binary;

  virtual void accept(Visitor& v) { v.visit(this); }
};


#endif // VISITCALC_H_INCLUDED
