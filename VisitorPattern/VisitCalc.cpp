#include "VisitCalc.h"

#include <iostream>

void EvalVisitor::visit(Int* e)
{
  ret = e->val;
}

void EvalVisitor::visit(Add* e)
{
  EvalVisitor v1;
  e->e1->accept(v1);

  EvalVisitor v2;
  e->e2->accept(v2);

  ret = v1.ret + v2.ret;
}

void EvalVisitor::visit(Sub* e)
{
  EvalVisitor v1;
  e->e1->accept(v1);

  EvalVisitor v2;
  e->e2->accept(v2);

  ret = v1.ret - v2.ret;
}

void EvalVisitor::visit(Mul* e)
{
  EvalVisitor v1;
  e->e1->accept(v1);

  EvalVisitor v2;
  e->e2->accept(v2);

  ret = v1.ret * v2.ret;
}

void EvalVisitor::visit(Div* e)
{
  EvalVisitor v1;
  e->e1->accept(v1);

  EvalVisitor v2;
  e->e2->accept(v2);

  ret = v1.ret / v2.ret;
}

int eval(Expr* e)
{
  struct V : Visitor
  {
    int ret;
    virtual void visit(Int* n)  { ret = n->val;}
    virtual void visit(Add* a)  { ret = eval(a->e1) + eval(a->e2); }
    virtual void visit(Sub* s)  { ret = eval(s->e1) - eval(s->e2); }
    virtual void visit(Mul* m)  { ret = eval(m->e1) * eval(m->e2); }
    virtual void visit(Div* d)  { ret = eval(d->e1) / eval(d->e2); }
  };
  V vis;
  e->accept(vis);
  return vis.ret;
}

void print(Expr* e)
{
  struct pVis : Visitor
  {
    virtual void visit(Int* n)  { std::cout <<  n->val; }
    virtual void visit(Add* a)
    {
        std::cout << "(";
        print(a->e1);
        std::cout << " + ";
        print(a->e2);
        std::cout << ") ";
    }
    virtual void visit(Sub* s)
    {
        std::cout << "(";
        print(s->e1);
        std::cout << " - ";
        print(s->e2);
        std::cout << ") ";
    }
    virtual void visit(Mul* m)
    {
        std::cout << "(";
        print(m->e1);
        std::cout << " * ";
        print(m->e2);
        std::cout << ") ";
    }
    virtual void visit(Div* d)
    {
        std::cout << "(";
        print(d->e1);
        std::cout << " / ";
        print(d->e2);
        std::cout << ") ";
    }
  };
  pVis vis;
  e->accept(vis);
}

//Not finished
Expr* clone(Expr* e)
{
  struct cVis : Visitor
  {
    virtual void visit(Int* n)  {}
    virtual void visit(Add* a)  {}
    virtual void visit(Sub* s)  {}
    virtual void visit(Mul* m)  {}
    virtual void visit(Div* d)  {}
  };
  cVis vis;
  e->accept(vis);
 // return Binary*();
}
