#ifndef VIRTUALCALC_H_INCLUDED
#define VIRTUALCALC_H_INCLUDED

#include <stdexcept>
#include <iostream>

struct VirtExpr;
struct Int;
struct Add;
struct Sub;
struct Mul;
struct Div;

//These implementations have been done using virtual functions.
struct VirtExpr
{
  virtual ~VirtExpr() = default;
  virtual void printExpr() const = 0;
  virtual int evaluate() const = 0;
  virtual VirtExpr* reduce() const = 0;
  virtual bool is_value() const
  {return false;}
  virtual VirtExpr* clone() const = 0;
};
bool operator==(const VirtExpr& e1, const VirtExpr& e2);

struct Int : VirtExpr
{
  Int(int n) : val(n) {}

  virtual Int* clone() const
  {return new Int(*this);}

  virtual void printExpr() const
  {std::cout << val;}

  virtual int evaluate() const
  {return val;}

  virtual VirtExpr* reduce() const
  {throw std::runtime_error("already reduced");}

  virtual bool is_value() const
  {return true;}

  int val;
};

struct Binary : VirtExpr
{
  Binary(const Binary& that): e1(that.e1->clone()), e2(that.e2->clone()) {}

  Binary(VirtExpr *e1, VirtExpr* e2) : e1(e1), e2(e2) {}

  virtual ~Binary()
  {
    delete e1;
    delete e2;
  }

  VirtExpr* e1;
  VirtExpr* e2;
};

struct Add : Binary
{
  using Binary::Binary;

  virtual Add* clone() const
  {return new Add(*this);}
  virtual void printExpr() const;

  virtual int evaluate() const
  {
    return e1->evaluate() + e2->evaluate();
  }

  virtual VirtExpr* reduce() const;
};

struct Sub : Binary
{
  using Binary::Binary;

  virtual Sub* clone() const
  {return new Sub(*this);}

  virtual void printExpr() const;
  virtual int evaluate() const
  {
    return e1->evaluate() - e2->evaluate();
  }

  virtual VirtExpr* reduce() const;

};

struct Mul : Binary
{
  using Binary::Binary;

  Mul* clone() const override
  {return new Mul(*this);}

  virtual void printExpr() const;
  virtual int evaluate() const
  {
    return e1->evaluate() * e2->evaluate();
  }

  virtual VirtExpr* reduce() const;
};

struct Div : Binary
{
  using Binary::Binary;

  virtual Div* clone() const
  {return new Div(*this);}

  virtual void printExpr() const;
  virtual int evaluate() const;
  virtual VirtExpr* reduce() const;
};
#endif // VIRTUALCALC_H_INCLUDED
