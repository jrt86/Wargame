#include <iostream>
#include "VirtualCalc.h"


int main()
{
//Virtual begin
  std::cout << "Example done with virtual functions:\n\n";

  VirtExpr* virtualTest1 = new Add(
    new Mul(new Int(5), new Int(20)),
    new Div(new Int(100), new Int(4)));
    virtualTest1->printExpr();
    std::cout << " = " << virtualTest1->evaluate() << "\n\n";

    std::cout << "Copy of previous:\n";
  VirtExpr* virtualCopy = virtualTest1->clone();
  virtualCopy->printExpr();
  std::cout << " = " << virtualCopy->evaluate() << "\n\n";


  delete virtualTest1;
  delete virtualCopy;

    return 0;
}
