#include "VirtualCalc.h"

int Div::evaluate() const
{
    int d = e2->evaluate();
    if (d == 0)
    {
      throw std::runtime_error("divide by zero");
    }
    return e1->evaluate() / d;
}

void Div::printExpr() const
{
    std::cout << "(";
    e1->printExpr();
    std::cout << " / ";
    e2->printExpr();
    std::cout << ")";
}

VirtExpr* Div::reduce() const
{
    if (e1->is_value())
    {
      if (e2->is_value())
      {
        return new Int(evaluate());
      }
      return new Div(e1->clone(), e2->reduce());
    }
    return new Div(e1->reduce(), e2->clone());
}

void Mul::printExpr() const
{
    std::cout << "(";
    e1->printExpr();
    std::cout << " * ";
    e2->printExpr();
    std::cout << ")";
}

VirtExpr* Mul::reduce() const
{
    if (e1->is_value())
    {
      if (e2->is_value())
      {
        return new Int(evaluate());
      }
      return new Mul(e1->clone(), e2->reduce());
    }
    return new Mul(e1->reduce(), e2->clone());
}

void Sub::printExpr() const
{
    std::cout << "(";
    e1->printExpr();
    std::cout << " - ";
    e2->printExpr();
    std::cout << ")";
}

VirtExpr* Sub::reduce() const
{
    if (e1->is_value())
    {
      if (e2->is_value())
      {
        return new Int(evaluate());
      }
      return new Sub(e1->clone(), e2->reduce());
    }
    return new Sub(e1->reduce(), e2->clone());
}

void Add::printExpr() const
{
    std::cout << "(";
    e1->printExpr();
    std::cout << " + ";
    e2->printExpr();
    std::cout << ")";
}

VirtExpr* Add::reduce() const
{
    if (e1->is_value())
    {
      if (e2->is_value())
      {
        return new Int(evaluate());
      }
      return new Add(e1->clone(), e2->reduce());
    }
    return new Add(e1->reduce(), e2->clone());
}

